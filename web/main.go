package main

import (
	"fmt"
	"microHello/web/handler"
	"net/http"

	"github.com/gin-gonic/gin"
)

const addr = ":8888"

func Index(c *gin.Context) {
	c.JSON(http.StatusOK, map[string]interface{}{
		"message": "index",
	})
}

func main() {
	r := gin.Default()
	r.Handle("GET", "/", Index)
	// mdns
	r.Handle("GET", "/hello", handler.ServiceOne)
	// consul
	r.Handle("GET", "/hello2", handler.ServiceOne2)
	// etcd
	r.Handle("GET", "/hello3", handler.ServiceOne3)
	if err := r.Run(addr); err != nil {
		fmt.Println("err")
	}
}
