package main

import (
	"microHello/service/hello/handler"
	pb "microHello/service/hello/proto"

	// "github.com/micro/micro/v3/service"
	// "github.com/micro/micro/v3/service/logger"
    service "github.com/asim/go-micro/v3" // "github.com/micro/micro/v3/service"
	"github.com/asim/go-micro/v3/logger"  // "github.com/micro/micro/v3/service/logger"
)

func main() {
	// Create service
    srv := service.NewService( // service.New
		service.Name("hello"),
        service.Version("latest"),
	)

	// Register handler
	// pb.RegisterHelloHandler(srv.Server(), handler.New())
    _ = pb.RegisterHelloHandler(srv.Server(), new(handler.Hello))

	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
