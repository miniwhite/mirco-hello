package handler

import (
	"context"

	// log "github.com/micro/micro/v3/service/logger"
	log "github.com/asim/go-micro/v3/logger"

	hello3 "microHello/service/hello3/proto"
)

type Hello3 struct{}

// Return a new handler
func New() *Hello3 {
	return &Hello3{}
}

// Call is a single request handler called via client.Call or the generated client code
func (e *Hello3) Call(ctx context.Context, req *hello3.Request, rsp *hello3.Response) error {
	log.Info("Received Hello3.Call request")
	rsp.Msg = "Hello " + req.Name
	return nil
}

// Stream is a server side stream handler called via client.Stream or the generated client code
func (e *Hello3) Stream(ctx context.Context, req *hello3.StreamingRequest, stream hello3.Hello3_StreamStream) error {
	log.Infof("Received Hello3.Stream request with count: %d", req.Count)

	for i := 0; i < int(req.Count); i++ {
		log.Infof("Responding: %d", i)
		if err := stream.Send(&hello3.StreamingResponse{
			Count: int64(i),
		}); err != nil {
			return err
		}
	}

	return nil
}

// PingPong is a bidirectional stream handler called via client.Stream or the generated client code
func (e *Hello3) PingPong(ctx context.Context, stream hello3.Hello3_PingPongStream) error {
	for {
		req, err := stream.Recv()
		if err != nil {
			return err
		}
		log.Infof("Got ping %v", req.Stroke)
		if err := stream.Send(&hello3.Pong{Stroke: req.Stroke}); err != nil {
			return err
		}
	}
}
