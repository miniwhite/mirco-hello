package main

import (
	"microHello/service/hello3/handler"
	pb "microHello/service/hello3/proto"

	"github.com/asim/go-micro/plugins/registry/etcd/v3"

	service "github.com/asim/go-micro/v3" // "github.com/micro/micro/v3/service"
	"github.com/asim/go-micro/v3/logger"  // "github.com/micro/micro/v3/service/logger"
	"github.com/asim/go-micro/v3/registry"
)

const (
	EtcdAddr   = "127.0.0.1:2379"
	ServerName = "hello3"
)

// etcd 作为注册中心
func main() {
	// 使用etcd作为注册
	etcdReg := etcd.NewRegistry(
		registry.Addrs(EtcdAddr),
	)
	// Create service
	// srv := service.NewService(
	// 	service.Name("hello3"),
	// )

	srv := service.NewService(
		service.Name(ServerName),  // 服务名字
		service.Registry(etcdReg), // 注册中心
	)

	// Register handler
	pb.RegisterHello3Handler(srv.Server(), handler.New())

	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
