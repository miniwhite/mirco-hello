# Hello3 Service

This is the Hello3 service

Generated with

```
micro new hello3
```

## Usage

Generate the proto code

```
make proto
```

Run the service

```
micro run .
```