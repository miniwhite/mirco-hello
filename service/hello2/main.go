package main

import (
	"microHello/service/hello2/handler"
	pb "microHello/service/hello2/proto"

	"github.com/asim/go-micro/plugins/registry/consul/v3"
	service "github.com/asim/go-micro/v3" // "github.com/micro/micro/v3/service"
	"github.com/asim/go-micro/v3/logger"  // "github.com/micro/micro/v3/service/logger"
	"github.com/asim/go-micro/v3/registry"
)

const (
	ServerName = "hello2"
	ConsulAddr = "127.0.0.1:8500"
)

// consul 作为注册中心
func main() {
	consulReg := consul.NewRegistry(
		registry.Addrs(ConsulAddr),
	)

	// Create service
	srv := service.NewService(
		service.Name(ServerName),    // 服务名字
		service.Registry(consulReg), // 注册中心
	)
	// Create service
	// srv := service.NewService( // service.New
	// 	service.Name("hello2"),
	// 	service.Version("latest"),
	// )

	// Register handler
	// pb.RegisterHelloHandler(srv.Server(), handler.New())
	_ = pb.RegisterHello2Handler(srv.Server(), new(handler.Hello2))

	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
