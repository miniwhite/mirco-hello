### 说明

clone 代码后:删除go.mod,自己重新生成
```
cd microHello
rm -f go.mod
go mod init microHello

// 启动mdns 微服务
cd service/hello
go run main.go
// web 访问:http:127.0.0.1:8888/hello?key=mdns

// 启动consul 微服务
// 本地先启动consul 
consul agent -dev

cd service/hello2
go run main.go
// web 访问:http:127.0.0.1:8888/hello2?key=consul

// 启动etcd 微服务
// 本地先启动etcd
etcd

cd service/hello3
go run main.go
// web 访问:http:127.0.0.1:8888/hello3?key=etcd

```

### 参考文档
https://zhuanlan.zhihu.com/p/545972420
